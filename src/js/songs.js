window.addEventListener('load', function() {
    var searchBox = document.getElementById('search-box')
    var articles = document.querySelectorAll('article');
    function cleanupFilter() {
        document.body.classList.remove('searching');
        let target = document.querySelector(':target');
        // Firefox doesn't supprt :target in printed pages
        if (target && target.id != 'start') {
            document.body.classList.add('targetted');
            target.classList.add('target');
        } else {
            document.body.classList.remove('targetted');
        }
        if (target) {
            target.scrollIntoView();
        }
    }
    function filter() {
        var search = searchBox.value;
        if (search == '') {
            document.body.classList.remove('searching');
        } else {
            document.body.classList.add('searching');

            for (var i = 0; i < articles.length; i++) {
                var article = articles[i];
                var heading = article.querySelector('h2');
                console.log(heading && heading.innerText.toLowerCase().indexOf(search.toLowerCase()) != -1)
                var matched = (heading && heading.innerText.toLowerCase().indexOf(search.toLowerCase()) != -1)
                article.classList.toggle('found', matched);
            }
        }
    }
    searchBox.addEventListener('input', filter);
    searchBox.addEventListener('search', filter);
    window.addEventListener('hashchange', cleanupFilter);
});
