const path = require('path')
const Metalsmith = require('metalsmith')
const chords = require('./metalsmith-chords')

let pipeline = Metalsmith(path.join(__dirname, '..'))
    .metadata({})
    .source('./src')
    .destination('./dist')
    .clean(true)
    .use(chords());

if (process.argv.indexOf('--dist') != -1) {
    const uglify = require('metalsmith-uglify');
    const cleanCSS = require('metalsmith-clean-css')
    const htmlMinifier = require("metalsmith-html-minifier");

    pipeline = pipeline
        .use(uglify({
            sameName: true,
            uglify: {
                sourceMap: false
            }
        }))
        .use(cleanCSS())
        .use(htmlMinifier())
}

if (process.argv.indexOf('--serve') != -1) {
    const watch = require('metalsmith-watch')
    const serve = require('metalsmith-serve')
    const injectLivereload = require('./metalsmith-inject-livereload')

    pipeline = pipeline
        .use(injectLivereload())
        .use(watch({
            paths: {
                "${source}/{js,css}/*": true,
                "${source}/**/*.chords": "songs/**/*",
            },
            livereload: true,
        }))
        .use(serve())
}

pipeline.build(function(err) {
    if (err) throw err
})
