const util = require('util')

const INDENT = {[util.inspect.custom]: () => "indent"}
const DEDENT = {[util.inspect.custom]: () => "dedent"}
const ENTER = {[util.inspect.custom]: () => "'['"}
const EXIT = {[util.inspect.custom]: () => "']'"}
const NEWLINE = {[util.inspect.custom]: () => "newline"}

function* tokenize(input) {
    let iterator = input[Symbol.iterator]()
    let current = iterator.next()
    let indentations = [{base: undefined, current: 0}]
    let currentText = ''
    let line = 0

    while (!current.done)
    {
        if (current.value == ':') {
            current = iterator.next()
            if (!current.done && current.value == '/') { // Utility for URI schemes like http://
                currentText += ':'
                continue
            }

            if (currentText != '') {
                yield [currentText, line]
                currentText = ''
            }

            while (!current.done && (current.value == ' ' || current.value == '\t')) {
                current = iterator.next()
            }

            let indentation = indentations[indentations.length - 1]
            indentations.push({
                base: indentation.current !== undefined ? indentation.current : indentation.base,
                current: undefined
            })

            yield [INDENT, line]

        } else if (current.value == '\n') {
            if (currentText != '') {
                yield [currentText, line]
                currentText = ''
            }

            let indent

            while (!current.done && (current.value == ' ' || current.value == '\t' || current.value == '\n')) {
                if (current.value == '\n') {
                    indent = 0
                    line ++
                } else {
                    indent++
                }
                current = iterator.next()
            }

            while (indentations[indentations.length - 1].base >= indent) {
                indentations.pop()
                yield [DEDENT, line]
            }

            yield [NEWLINE, line]

            let indentation = indentations[indentations.length - 1]
            if (indentation.current == undefined) {
                indentation.current = indent
            }
        } else if (current.value == '[') {
            current = iterator.next()

            if (currentText != '') {
                yield [currentText, line]
                currentText = ''
            }

            yield [ENTER, line]
        } else if (current.value == ']') {
            current = iterator.next()

            if (currentText != '') {
                yield [currentText, line]
                currentText = ''
            }

            yield [EXIT, line]
        } else {
            if (current.value == '\\') {
                current = iterator.next()
            }
            if (!current.done) {
                currentText += current.value
                current = iterator.next()
            }
        }
    }

    if (currentText != '') {
        yield [currentText, line]
        currentText = ''
    }

    while (indentations.length > 1) {
        indentations.pop()
        yield [DEDENT, line]
    }
}

const EOF = {[util.inspect.custom]: () => "EOF"}

function parse(filename, tokens, callbacks) {
    let iterator = tokens[Symbol.iterator]()

    let position = 0
    let current
    function advance() {
        let result = iterator.next()
        ;[current, position] = (result.done ? [EOF, position] : result.value)
    }
    advance()

    function match(...args) {
        for (var arg of args) {
            if (current == arg[0] || (arg[0] == undefined && typeof(current) == 'string')) {
                let previous = current
                if (typeof(arg[1]) == 'boolean' ? arg[1] : true) advance()
                return typeof(arg[1]) == 'function' ? arg[1](previous) : arg[1]
            }
        }
        var expected = args.map(x => x[0] != undefined ? util.inspect(x[0]) : 'text')
        if (expected.length > 1) expected[expected.length-1] = 'or ' + expected[expected.length-1]
        var expectedString = expected.join(expected.length > 2 ? ', ' : ' ')
        throw new Error('Unexpected ' + util.inspect(current) + ', expected ' + expectedString)
    }

    function parseField(fieldName) {
        match([INDENT])
        let result = match([undefined, callbacks[fieldName]])
        match([DEDENT])
        return [fieldName, result]
    }

    function invokeDirective(directive, lines, callbacks) {
        let m
        if ((m = directive.match(/^x([0-9]+)$/)) || (m = directive.match(/^([0-9]+)x$/)) && callbacks.repeat) {
            return callbacks.repeat(m[1], lines)
        }
        if (directive == 'section' && callbacks.section) {
            return callbacks.section(lines)
        }
        throw new Error('Unknown directive ' + directive)
    }

    function parseRepeat() {
        let lines = parseRepeatLines()
        return ['content', callbacks.repeat.full(lines)]
    }

    function parseRepeatLines() {
        match([INDENT])
        let lines = []
        while (current != DEDENT) {
            var line = parseRepeatLine()
            if (line !== undefined) lines.push(line)
        }
        match([DEDENT])
        return lines
    }

    function parseRepeatLine() {
        let result
        if (current != NEWLINE && current != DEDENT) {
            result = match([undefined, text => {
                if (current == INDENT) {
                    let sublines = parseRepeatLines()
                    return invokeDirective(text, sublines, callbacks.repeat)
                } else {
                    return callbacks.repeat.section(text)
                }
            }])
        }
        match([NEWLINE], [DEDENT, false])
        return result
    }

    function parseVerse(sectionName) {
        let lines = parseVerseLines()
        return ['content', callbacks.verse.full(sectionName, lines)]
    }

    function parseVerseLines() {
        match([INDENT])
        let lines = []
        while (current != DEDENT) {
            var line = parseVerseLine()
            if (line !== undefined) lines.push(line)
        }
        match([DEDENT])
        return lines
    }

    function parseVerseLine() {
        let lineParts = []
        let invokedDirective = false
        while (current != NEWLINE && current != DEDENT) {
            let result = match(
                [undefined, text => {
                    if (current == INDENT && !invokedDirective) {
                        var sublines = parseVerseLines()
                        lineParts = [invokeDirective(text, sublines, callbacks.verse)]
                        invokedDirective = true
                    } else {
                        lineParts.push(callbacks.verse.text(text))
                    }
                }],
                [ENTER, () => {
                    lineParts.push(match([undefined, callbacks.verse.chord]))
                    match([EXIT])
                }],
                [NEWLINE],
                [DEDENT, false]
            )
        }
        match([NEWLINE], [DEDENT, false])
        return invokedDirective ? lineParts[0] : lineParts.length > 0 ? callbacks.verse.line(lineParts) : undefined
    }

    function parseLinks() {
        match([INDENT])
        let links = []
        while (current != DEDENT) {
            match(
                [undefined, text => links.push(parseLink(text))],
                [NEWLINE])

        }
        match([DEDENT])
        return ['links', callbacks.links.full(links)]
    }

    function parseLink(linkName) {
        match([INDENT])
        let text = match([undefined, callbacks.links.text], [ENTER, false])
        let url = match([ENTER, () => {
            let url = match([undefined, callbacks.links.url])
            match([EXIT])
            match([DEDENT])
            return url
        }], [DEDENT])
        return callbacks.links.link(linkName, text, url)
    }

    function parseNotes() {
        match([INDENT])
        let notes = []
        while (current != DEDENT) {
            match(
                [undefined, text => notes.push(text)],
                [NEWLINE])

        }
        match([DEDENT])
        return ['notes', callbacks.notes(notes)]
    }

    function parseMain() {
        let parts = {}
        while (current != EOF) {
            let toplevel = match(
                ['title', parseField],
                ['notes', parseNotes],
                ['repeat', parseRepeat],
                ['links', parseLinks],
                [undefined, text => {
                    if (current == INDENT) {
                        return parseVerse(text)
                    } else {
                        return ['text', callbacks.freeform(text)]
                    }
                }],
                [NEWLINE],
                [EOF]
            )

            if (toplevel !== undefined) {
                parts[toplevel[0]] = parts[toplevel[0]] || []
                parts[toplevel[0]].push(toplevel[1])
            }
        }
        return callbacks.main(parts)
    }

    try {
        return parseMain()
    } catch(e) {
        e.message += ', at ' + filename + ':' + position
        throw e
    }
}

module.exports = function(filename, fileIterator, callbacks) {
    return parse(filename, tokenize(fileIterator), callbacks)
};
