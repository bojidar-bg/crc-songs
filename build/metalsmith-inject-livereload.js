function metalsmithPlugin(options = {}) {
    let scriptTag = '<script src="http://localhost:' + (options.port || 35729) + '/livereload.js"></script>'
    return function(files, metalsmith, done){
        setImmediate(done);

        for (var file of Object.keys(files)) if (file.match(/\.html$/)) {
            files[file].contents = Buffer.from(files[file].contents.toString('utf8').replace(/(<\/body>)/, scriptTag + '$1'));
        }
    };
}

module.exports = metalsmithPlugin
